import 'dog.dart';
import 'dog_dao.dart';
import 'cat.dart';
import 'cat_dao.dart';

void main() async {
  

  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);

  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);
  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age+7);
  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  /* Cat Class */

  var garfild = Cat(id: 0, name: 'Garfild', age: 7);
  var normal = Cat(id: 1, name: 'Normal', age: 2);

  await CatDao.insertCat(garfild);
  await CatDao.insertCat(normal);
  print(await CatDao.cats());

  garfild = Cat(id: garfild.id, name: garfild.name, age: garfild.age+3);
  await CatDao.updateDog(garfild);
  print(await CatDao.cats());

  await CatDao.deleteCat(1);
  print(await CatDao.cats());
}
